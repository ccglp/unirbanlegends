﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTest : MonoBehaviour {

	protected TestType testType; 

	public TestType TestType {
		get {
			return testType;
		}
		set {
			testType = value;
		}
	}
}
