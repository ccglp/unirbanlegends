﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAllUpdatesList : BehaviourTest {
	private List<Transform> transforms;



	// Update is called once per frame
	void Update () {
		for (int i = 0; i < transforms.Count; i++)
		{
			
			switch (testType) {
			case TestType.VectorRight:
				transforms[i].position += Vector3.right * Time.deltaTime;

				break; 

			case TestType.RotationConstant:
				transforms [i].eulerAngles += Vector3.right * Time.deltaTime; 
				break; 
			}
		}
	}

	public List<Transform> Transforms
	{
		get { return transforms; }
		set { transforms = value; }
	}
}
