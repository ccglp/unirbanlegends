﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

public enum TestMode
{
	ObjectUpdates,
	ControllerUpdates,
	ControllerUpdatesList,
	ControllerUpdatesForEach,
	Physics, 
	DoTween,
	Itween
}

public enum TestType{
	VectorRight, 
	RotationConstant

}
public class Controller : MonoBehaviour
{
    

	private List<float> times; 

    [SerializeField] private int cubeNumber;
    [SerializeField] private GameObject cube;
    [SerializeField] private TestMode testMode; 
	[SerializeField] private TestType testType; 
    void Awake () {
       
        List<Transform> transforms = new List<Transform>();
		times = new List<float> (); 


        for (int i = 0; i < cubeNumber; i++)
        {
            GameObject aux = ((GameObject)Instantiate(cube, new Vector3(i, 0, 0), Quaternion.identity));

            switch (testMode)
            {
                    case TestMode.ObjectUpdates:
						aux.AddComponent<MoveGameObject>().TestType = testType; 
                        break;

					case TestMode.ControllerUpdatesForEach:
					case TestMode.ControllerUpdatesList:
					case TestMode.ControllerUpdates :
						transforms.Add (aux.transform);
						
		                     
						break;
			

                    case TestMode.DoTween:
						switch(testType){
							case TestType.VectorRight:
								aux.transform.DOMoveX (10000, 10000); 
										break; 
							case TestType.RotationConstant:
								aux.transform.DORotate (new Vector3 (240, 0, 0), 240); 
										break; 
						}
                        break;

                    case TestMode.Itween:
					switch(testType){
						case TestType.VectorRight:
							iTween.MoveBy (aux, aux.transform.position + Vector3.right * 100000, 100000);
							break; 
						case TestType.RotationConstant:
							iTween.RotateBy (aux, new Vector3(240,0,0), 240); 
							break; 
					}

                    break;

					case TestMode.Physics:
						switch (testType) {
							case TestType.RotationConstant:
								aux.AddComponent<Rigidbody> ().angularVelocity =  (Vector3.right); 
								aux.GetComponent<Rigidbody> ().useGravity = false; 

								break;

							case TestType.VectorRight:
								aux.AddComponent<Rigidbody> ().velocity = Vector3.right; 
								aux.GetComponent<Rigidbody> ().useGravity = false; 

								break; 
						}
					break; 
                default:
                   
                    break; 
            }
        }

        switch (testMode)
        {
				case TestMode.ControllerUpdates:
					this.gameObject.AddComponent<MoveAllUpdate> ().Transforms = transforms.ToArray ();
					this.gameObject.GetComponent<MoveAllUpdate> ().TestType = testType; 
		        break;

				case TestMode.ControllerUpdatesForEach:
					this.gameObject.AddComponent<MoveAllUpdatesForEach> ().Transforms = transforms; 
					this.gameObject.GetComponent<MoveAllUpdatesForEach> ().TestType = testType; 
				break; 
				
				case TestMode.ControllerUpdatesList:
					this.gameObject.AddComponent<MoveAllUpdatesList> ().Transforms = transforms; 
					this.gameObject.GetComponent<MoveAllUpdatesList> ().TestType = testType; 
				break; 

        
                   
            default:
                break; 
        }

	}
	
	void Update(){
		times.Add (Time.deltaTime); 

	}

	void OnDestroy(){
		float timeAccumulate = 0; 
		int frames = 0; 
		for (int i = 5; i < times.Count-5; i++) {
			timeAccumulate += times [i]; 
			frames++; 
		}

		print("Time : " + testMode.ToString() + " "  + ((timeAccumulate / frames) * 1000).ToString()); 


	}
}
