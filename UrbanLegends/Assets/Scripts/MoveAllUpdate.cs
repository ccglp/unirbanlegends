﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAllUpdate : BehaviourTest
{
	private Transform[] transforms;

   

    // Update is called once per frame
	void Update () {
		for (int i = 0; i < transforms.Length; i++)
	    {
			switch (testType) {
			case TestType.VectorRight:
				transforms[i].position += Vector3.right * Time.deltaTime;

				break; 

			case TestType.RotationConstant:
				transforms [i].eulerAngles += Vector3.right * Time.deltaTime; 
				break; 
			}
	    }
	}

    public Transform[] Transforms
    {
        get { return transforms; }
        set { transforms = value; }
    }
}
