﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAllUpdatesForEach : BehaviourTest {

	private List<Transform> transforms;

	void Start(){
	}
	// Update is called once per frame
	void Update () {
		foreach(Transform aux in transforms)
		{			
			switch (testType) {
			case TestType.VectorRight:
				aux.transform.position += Vector3.right * Time.deltaTime; 

				break; 

			case TestType.RotationConstant:
				aux.transform.eulerAngles += Vector3.right * Time.deltaTime; 
				break; 
			}
		}



	}



	public List<Transform> Transforms
	{
		get { return transforms; }
		set { transforms = value; }
	}
}
